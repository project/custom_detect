
README.txt
==========
********************************************************************
This is custom_detect package 7.x, and will work with Drupal 7.x
********************************************************************
Browser Based Targeting:- 
This modules provide the flexibility to show block 
in a region based on different browser,
like Internet Explorer, Mozila FireFox, 
Google Chrome etc.
These modules will build onto Drupal 7 core
features enabling a browser based targetingAdditional Support
=================
For support, please create a support request for this 
module's project:
https://drupal.org/user/149580
Support questions by email to the module maintainer 
will be simply ignored. Use the issue tracker.
====================================================================
Current Maintainer: Ajesh Prakash <aajeshprakash007@gmail.com>,
drupal at https://drupal.org/user/149580
